/**
 * Created by Max on 24/05/2017.
 */

namespace App {
    declare function require (name: string): any;

    declare let __dirname: string;
    declare let module: any;
    declare let global: any;

    global.__base = __dirname;

    const express = require('express');
    const path = require('path');
    const favicon = require('serve-favicon');
    const logger: any = require('morgan');
    const cookieParser: any = require('cookie-parser');
    const bodyParser = require('body-parser');
    const sassMiddleware = require('node-sass-middleware');
    const mongoose = require('mongoose');
    mongoose.Promise = global.Promise;
    const configDB = require(`${path.join(global.__base, 'config')}/db`);
    const passport = require('passport');
    const LocalStrategy = require('passport-local').Strategy;
    const session = require('express-session');
    const i18n = require('i18n');
    const busboy = require('connect-busboy');
    const passportConfig = require(`${path.join(global.__base, 'config')}/passportjs`);
    const User = require(`${global.__base}/schemas/user`);

    mongoose.connect(configDB.url, function (err: any) {
            if (err) {
                throw err;
            }
        },
    );

    const app = express();

// view engine setup
    app.set('views', path.join(__dirname, 'views'));
    app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
// app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
    app.use(logger('dev'));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: false}));
    app.use(cookieParser());
    app.use(busboy());
    app.use(sassMiddleware({
        debug: true,
        dest: path.join(__dirname, 'public'),
        indentedSyntax: false, // true = .sass and false = .scss
        sourceMap: true,
        src: path.join(__dirname, 'public'),
        log: (severity: any, key: any, value: any) => {
            console.log('TESTING', key);
            // console.log(severity, 'node-saas-middleware   %s : %s', key, value);
        }
    }));

// PassportJS ============
    app.use(session({
        secret: 'PleaseChangeMe',
        resave: true,
        saveUninitialized: true
    }));
    app.use(passport.initialize());
    app.use(passport.session()); // persistent login sessions
// passport.use(User.createStrategy());
    passport.use(new LocalStrategy(User.authenticate(), {usernameField: 'email'}));
    passport.serializeUser(User.serializeUser());
    passport.deserializeUser(User.deserializeUser());

// i18n ==================
    i18n.configure({
        locales: ['nl', 'en'],
        directory: path.join(__dirname, 'locales'),
        // you may alter a site wide default locale
        defaultLocale: 'en',

        // sets a custom cookie name to parse locale settings from - defaults to NULL
        cookie: 'language',

        // query parameter to switch locale (ie. /home?lang=ch) - defaults to NULL
        queryParameter: 'lang',
        api: {
            __: 'i18n',  // now req.__ becomes req.t
            __n: 'i18ns' // and req.__n can be called as req.tn
        },
        objectNotation: true
    });
    app.use(i18n.init);

// ROUTES ================
    const index = require('./routes/index');
    const users = require('./routes/users');

    app.use('/data', passportConfig.authenticationMiddleware);
    app.use('/data', express.static(path.join(__dirname, 'data')));
    app.use(express.static(path.join(__dirname, 'public')));
    app.use('/libs', express.static(path.join(__dirname, 'node_modules')));

    app.use('/', index);
    app.use('/users', users);

// catch 404 and forward to error handler
    app.use(function (req: any, res: any, next: any) {
        const err: any = new Error('Not Found');
        err.status = 404;
        next(err);
    });

// error handler
    app.use(function (err: any, req: any, res: any, next: any) {
        // set locals, only providing error in development
        res.locals.message = err.message;
        res.locals.error = req.app.get('env') === 'development' ? err : {};

        // render the error page
        const statusCode: number = err.status || 500;
        res.status(statusCode);
        const pageData: any = {
            title: res.i18n(`error.${statusCode}.title`),
            message: res.i18n(`error.${statusCode}.message`)
        };

        res.render('error', pageData);
    });

    module.exports = app;
}