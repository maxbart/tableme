/**
 * Created by Max on 25/05/2017.
 */

let User = require(`${__base}/schemas/user`);

let local = User.createStrategy();

let authenticationMiddleware = function (req, res, next) {
    if (req.user) {
        if (req.isAuthenticated()) {
            if (req.user.enabled) {
                return next();
            } else {
                res.redirect('/logout');
            }
        } else {
            console.error("NOT AUTHENTICATED");
            res.redirect('/');
        }
    } else {
        console.error("NO USER PRESENT");
        res.redirect('/');
    }
};


module.exports = {
    local: local,
    authenticationMiddleware: authenticationMiddleware
};
