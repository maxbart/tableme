interface IUserData {
    [key: string]: any;
    firstName?: string;
    lastName?: string;
    email?: string;
    photo?: string;
    password?: string;
    passwordCheck?: string;
}