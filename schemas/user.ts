namespace UserModel {
    declare const module: any;
    declare function require (name: string): any;

    const mongoose = require('mongoose');
    const passportLocalMongoose = require('passport-local-mongoose');

    const userSchema = new mongoose.Schema({
        createdAt: {type: Date, default: Date.now},
        updatedAt: {type: Date, default: Date.now},
        enabled: {type: Boolean, default: true},
        email: {type: String, required: true, unique: true, lowercase: true, trim: true},
        role: {type: String},
        dateOfBirth: {type: Date},
        photo: {type: String},
        firstName: {type: String, required: true},
        lastName: {type: String, required: true},
        gender: {type: String},
        restaurantHolder: {type: Boolean, default: false},
        restaurants: [String],
        contact: {
            address: {
                street: {type: String},
                houseNumber: {type: Number},
                houseNumberAddition: {type: String},
                zipcode: {type: String},
                city: {type: String},
                country: {type: String}
            },
            phone: {type: String}
        }
    });
    userSchema.plugin(passportLocalMongoose, {usernameField: 'email'});

    module.exports = mongoose.model('User', userSchema);

}
