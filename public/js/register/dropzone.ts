/**
 * Created by Max on 27/05/2017.
 */
Dropzone.autoDiscover = false;

$('#profile-image').dropzone({
    url: '/register',
    previewsContainer: '#dropzonePreview',
    maxFiles: 1,
    async: false,
    maxFilesize: 2, //mb
    acceptedFiles: 'image/*',
    addRemoveLinks: false,
    autoProcessQueue: false,// used for stopping auto processing uploads
    autoDiscover: false,
    success: function (file, response) {
        if (response.success && response.relocate && response.user){
            $.post( response.relocate, response.user)
                .done(function( data ) {
                    window.location.href='/users'; //TODO: return relocation URL from login method
                });
        }
        console.log(response);
    },
    previewTemplate: `<div class="dz-image-preview hide">                        
                            <img data-dz-thumbnail />
                      </div>`,
    init: function () {
        let profileDropzone = this;

        profileDropzone.on("maxfilesexceeded", function (file) {
            this.removeAllFiles();
            this.addFile(file);
        });
        profileDropzone.on("thumbnail", function (file, dataUrl) {
            $('#profile-image').css({
                'background-image': 'url(' + dataUrl + ')',
                'background-size': 'cover'
            });
        });


        // First change the button to actually tell Dropzone to process the queue.
        $("#registration-form").submit(function (e) {
            // Make sure that the form isn't actually being sent.
            let form = $('#registration-form');

            profileDropzone.options.params = {
                'firstName': form.find('[name=firstname]').val(),
                'lastName': form.find('[name=lastname]').val(),
                'email': form.find('[name=email]').val(),
                'password': form.find('[name=password]').val(),
                'passwordCheck': form.find('[name=passwordCheck]').val()
            };
            e.preventDefault();
            e.stopPropagation();
            if (profileDropzone.getQueuedFiles().length > 0) {
                profileDropzone.processQueue();
            } else {
                profileDropzone.uploadFiles([{name: null}]); //send empty
            }
        });


    }
});