$(function () {
    $(window).scroll(function () {
        if ($("header").is(":visible")) {
            var winTop = $(window).scrollTop();
            if (winTop >= 30) {
                $("body").addClass("sticky-shrinknav-wrapper");
            } else {
                $("body").removeClass("sticky-shrinknav-wrapper");
            }
        } else {
            $("body").removeClass("sticky-shrinknav-wrapper");
        }
    });

    // $("header").is(':visible');
    $(window).resize(function(){
        if ($("header").is(':visible')){
            $("body").addClass("navbar-offset");
        } else {
            $("body").removeClass("navbar-offset");
            $("#login-canvas").foundation('close');

        }
    });
    if ($("header").is(':visible')){
        $("body").addClass("navbar-offset");
    } else {
        $("body").removeClass("navbar-offset");
        $("#login-canvas").foundation('close');

    }
});
