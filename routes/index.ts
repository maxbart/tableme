/**
 * Created by Max on 25/05/2017.
 */
namespace Index {
    declare const __base: string;
    declare const module: any;

    const express = require('express');
    const router = express.Router();
    const User = require(`${__base}/schemas/user`);
    const uuid = require('node-uuid');
    const fs = require('fs');
    const path = require('path');
    const shell = require('shelljs');
    const passport = require('passport');
    const Busboy = require('busboy');

    router.get('/', function (req: any, res: any, next: any) {
        const pageData: any = {
            title: res.i18n('title'),
            user: req.user ? null : req.user
        };
        res.render('index', pageData);
    });

    router.get('/login', function (req: any, res: any, next: any) {
        const pageData: any = {
            appTitle: res.i18n('title'),
            title: res.i18n('login.title'),
            user: req.user ? null : req.user
        };

        res.render('./login', pageData);
    });

    router.get('/logout', (req: any, res: any, next: any) => {
        req.logout();
        res.redirect('/');
    });

    router.get('/register', function (req: any, res: any, next: any) {
        const pageData: any = {
            appTitle: res.i18n('title'),
            title: res.i18n('register.title'),
            user: req.user ? null : req.user
        };

        res.render('./register', pageData);
    });

    router.post('/login', function (req: any, res: any, next: any) {
        User.authenticate()(req.body.email, req.body.password, function (err: any, user: any, options: any) {
            if (err) {
                return next(err);
            }
            if (user === false) {
                res.send({
                    message: options.message,
                    success: false
                });
            } else {
                req.login(user, function (loginErr: any) {
                    if (loginErr) {
                        console.error(loginErr);
                    }

                    res.redirect('/users');
                });
            }
        });

    });

    router.post('/register', function (req: any, res: any, next: any) {
        const busboy: any = new Busboy({headers: req.headers});
        const userData: IUserData = {};
        let dataSets = 0;

        busboy.on('field', function (key: string, value: any, keyTruncated: any, valueTruncated: any) {
            dataSets++;
            userData[key] = value;
            dataSets--;
        });

        busboy.on('file', function (fieldname: string, file: any, filename: string) {
            dataSets++;
            const relativeDir: string = '/data/images/users';
            const uploadDir: string = `${__base}${relativeDir}`;
            if (!fs.existsSync(uploadDir)) {
                shell.mkdir('-p', uploadDir);
            }

            const newFilename: string = uuid.v4() + path.extname(filename);

            const stream: any = fs.createWriteStream(uploadDir + '/' + newFilename);
            file.pipe(stream);
            stream.on('close', function () {
                console.log('File ' + filename + ' is uploaded as ' + newFilename);
            });
            stream.on('finish', function () {
                userData.photo = `${relativeDir}/${newFilename}`;
                dataSets--;
            });
        });

        busboy.on('finish', function () {
            if (dataSets === 0) {
                if (userData.password !== userData.passwordCheck) {
                    return 'error';
                }
                User.register(new User({
                        firstName: userData.firstName,
                        lastName: userData.lastName,
                        email: userData.email,
                        photo: userData.photo ? userData.photo : ''
                    }),
                    userData.password, function (err: any, user: any) {

                        if (err) {
                            return {error: err.name};
                        }
                        User.authenticate()(
                            user.email,
                            userData.password,
                            function (authError: any, authUser: any, options: any) {
                                if (authError) {
                                    return next(authError);
                                }
                                if (authUser === false) {
                                    res.json({
                                        message: options.message,
                                        success: false
                                    });
                                } else {
                                    req.login(authUser, function (loginErr: any) {
                                        if (loginErr) {
                                            next(loginErr);
                                        }
                                        const successData: any = {
                                            success: true,
                                            relocate: '/login',
                                            user: {
                                                email: user.email,
                                                password: userData.password
                                            }
                                        };
                                        res.json(successData);
                                    });
                                }
                                res.end('');
                            });
                    });
            }
        });

        return req.pipe(busboy);
    });

    module.exports = router;
}