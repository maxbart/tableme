/**
 * Created by Max on 25/05/2017.
 */

namespace Users {
    declare const module: any;

    const express = require('express');
    const router = express.Router();
    const passportConfig = require('../config/passportjs');

    router.use(passportConfig.authenticationMiddleware);

    /* GET users listing. */
// testing
    router.get('/', function (req: any, res: any, next: any) {
        const pageData: any = {
            appTitle: res.i18n('title'),
            title: 'USER PAGE',
            user: req.user
        };
        res.render('./users/index', pageData);
    });

    module.exports = router;
}
